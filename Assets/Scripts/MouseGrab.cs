﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseGrab : MonoBehaviour {

	private GameObject planet;
	private DetectPlanet detectPlanetScript;
	public bool grabing = false;
	private GameObject grabedObject;
	public GameObject crossair;
	private SpriteRenderer crossairSpr;
	private bool canDrop = false;
	public LayerMask crossairLayerMask;
	public LayerMask grabLayerMask;

	GameManager gameManager;

	void Start(){
		planet = GameObject.FindGameObjectWithTag ("Earth");
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();

		if (crossair != null) {
			crossairSpr= crossair.GetComponentInChildren<SpriteRenderer>();
		}
	}


	void FixedUpdate () {
		
		GrabObject ();
		if (grabing) {
			if(gameManager.gameClickState==ClickStates.ClickDrop)
				CrossairMovement ();
		} else {
			if (crossairSpr != null) {
				crossairSpr.color = new Color (1f, 1f, 1f, 0f);
			}
		}
	}

	private void CrossairMovement(){
		if (crossair != null) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			ray = new Ray (ray.origin + transform.forward, ray.direction);
			RaycastHit hit;
			Debug.DrawRay (Input.mousePosition, transform.forward,Color.green);
			if (Physics.Raycast (ray, out hit, 120f,crossairLayerMask)) {
				if (hit.transform.gameObject.CompareTag ("Earth")) {
					canDrop = true;
					PlanetGravity planetGravity = planet.GetComponent<PlanetGravity> ();
					crossair.transform.position = hit.point;
					planetGravity.RotateToPlanet (crossair.transform);
					if (crossairSpr != null) {
						crossairSpr.color = new Color (1f, 1f, 0f, 1f);
					}
				} else if (hit.transform.gameObject.CompareTag ("Water")) {
					if (grabedObject.layer == LayerMask.NameToLayer ("Orbitter")) {
						canDrop = true;
						if (crossairSpr != null) {
							crossairSpr.color = new Color (1f, 1f, 0f, 1f);
						}
					} else {
						canDrop = false;
						if (crossairSpr != null) {
							crossairSpr.color = new Color (1f, 0f, 0f, 1f);
						}
					}
					PlanetGravity planetGravity = planet.GetComponent<PlanetGravity> ();
					crossair.transform.position = hit.point;
					planetGravity.RotateToPlanet (crossair.transform);
				}
			}
		}
	}

	private void GrabObject(){
		bool ctrlPressed = false;//Input.GetKey (KeyCode.LeftControl);
		if (Input.GetButtonDown ("Fire1")) {
			if (!ctrlPressed) {
				if (!grabing) {
					if (gameManager.gameClickState == ClickStates.ClickInstanciate) {
						Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
						RaycastHit hit;
						if (Physics.Raycast (ray, out hit, 100, grabLayerMask)) {
							grabedObject = hit.transform.gameObject;
							detectPlanetScript = grabedObject.GetComponent<DetectPlanet> ();
							if (detectPlanetScript != null) {
								grabing = true;
								detectPlanetScript.grabbed = true;
								gameManager.gameClickState = ClickStates.ClickDrop;
							} else {
								grabedObject = null;
							}
						}
					}
				} else {
					if (canDrop) {
						grabing = false;
						if (detectPlanetScript != null) {
							detectPlanetScript.grabbed = false;
							detectPlanetScript.targetAquired = true;
							detectPlanetScript.targetPoint = crossair.transform.position;
						}
					} else {
						//Reproducir Sonido de error

					}
				}
			}/* else {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, 120,grabLayerMask)) {
					Debug.Log ("asdfasdf");
					if (hit.transform.gameObject.CompareTag ("Antenna")) {
						Debug.Log ("asdfasdf2222");
						AntennaController aC = hit.transform.gameObject.GetComponent<AntennaController> ();
						aC.ChangeState ();
						gameManager.gameClickState = ClickStates.ClickAntennaView;
					}
				}
			}*/
		}

		if (grabing && grabedObject!=null) {
			Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5f);
			grabedObject.transform.position = Camera.main.ScreenToWorldPoint (mousePos);
		}
	}

	public void SetGrabbedObject(GameObject grabThis){
		grabedObject = grabThis;
		detectPlanetScript = grabedObject.GetComponent<DetectPlanet> ();
		if (detectPlanetScript != null) {
			grabing = true;
			detectPlanetScript.grabbed = true;
			gameManager.gameClickState = ClickStates.ClickDrop;
		} else {
			grabedObject = null;
		}
	}
}
