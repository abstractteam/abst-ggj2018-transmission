﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody))]

public class DetectPlanet : MonoBehaviour {

	private PlanetGravity planet;
	private Rigidbody myRB;
	//private float fixedZ = 0f;

	[Range(0f,20f)]
	public float velocity = 0f;
	public bool grabbed;

	[Range(0f,10f)]
	public float grabbedGravity = 0.1f;
	[Range(0f,10f)]
	public float gravity = 9.8f;

	public Vector3 targetPoint;
	public bool targetAquired = false;
	private Renderer[] rend;
	public GameManager gameManager;

	void Awake() 
	{
		myRB = GetComponent<Rigidbody> ();
		rend = GetComponentsInChildren<Renderer> ();
		myRB.useGravity = false;
		myRB.constraints = RigidbodyConstraints.FreezeRotation;
	}

	void Start()
	{
		planet = GameObject.FindGameObjectWithTag("Earth").GetComponent<PlanetGravity>();
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
	}

	void FixedUpdate ()
	{
		if (!grabbed && targetAquired) {
			SetRenderAlpha(1f);
			planet.Attract (transform, targetPoint, gravity);
		} else if (grabbed) {
			
			SetRenderAlpha (0.5f);
			planet.RotateToPlanet (transform);
			myRB.velocity = Vector3.zero;
		} else {
			myRB.velocity = Vector3.zero;
		}
	}

	void OnCollisionEnter(Collision col){
		if (!grabbed && targetAquired) {
			if (col.gameObject.CompareTag ("Earth")) {
				//emitt Particle
				targetAquired = false;
				GameObject smoke = Instantiate (Resources.Load ("LandSmoke")) as GameObject;
				smoke.transform.position = col.contacts [0].point;
			}
			if( col.gameObject.CompareTag ("Earth") || col.gameObject.CompareTag ("Orbit")){
				gameManager.gameClickState = ClickStates.ClickInstanciate;
				//this.enabled = false;
			}
		}
	}

	void SetRenderAlpha(float newAlpha){
		foreach (Renderer r in rend) {
			r.material.color = new Color (r.material.color.r, r.material.color.g, r.material.color.b, newAlpha);
		}
	}
}