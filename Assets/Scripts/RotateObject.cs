﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

	[Range(0.01f,1f)]
	public float rotateVelocity;

	void Update () {
		transform.Rotate (new Vector3(0f,0f,rotateVelocity));
	}
}
