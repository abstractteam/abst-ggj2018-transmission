﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MissileController : MonoBehaviour {

    public enum MissileState
    {
        Idle,
        Launch,
        Crash
    }

    public MissileState missilState;

    Animator anim; 
    
    private Vector3 lastPosition;

    GameManager gameManager;

    public bool testLaunch;
    public GameObject endVideo;

    // Use this for initialization
    void Start () {
        missilState = MissileState.Idle;
        anim = GetComponent<Animator>();
       // gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}

    void Update()
    {
        if (testLaunch) {
            LaunchMissile();
            testLaunch = false;
        }

        if(missilState == MissileState.Launch)
        {
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("MissileEnd"))
            {
                DestroyWorld();
                missilState = MissileState.Crash;
            }
        }
    }
    
    public void LaunchMissile()
    {
        anim.SetTrigger("Launch");
        missilState = MissileState.Launch;
    }
   
    //called by animation event 
    void DestroyWorld()
    {
        GameObject video = Instantiate(endVideo);
        //Quaternion originalRotation = endVideo.transform.localRotation;
        Vector3 cameraPosition = video.transform.Find("CameraPosition").transform.position;
        Camera.main.transform.position = cameraPosition;
        Camera.main.transform.rotation = Quaternion.Euler(0, 0, 0);

        video.transform.parent = Camera.main.transform;
        
//        endVideo.transform.parent = Camera.main.transform;
  //      endVideo.SetActive(true);        
    }

}
