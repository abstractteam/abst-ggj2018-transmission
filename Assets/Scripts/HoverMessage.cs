﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverMessage : MonoBehaviour {

    public GameObject helpMessage;
    
    public void MouseEnterButton()
    {
      //  Debug.Log("mouse enter!");
        helpMessage.SetActive(true);        
    }
    
    public void MouseExitButton()
    {
        //Debug.Log("mouse exit!");
        helpMessage.SetActive(false);
    }  

    public void OnDisable()
    {
        //Debug.Log("i'm disabled!");
        helpMessage.SetActive(false);
    }
}
