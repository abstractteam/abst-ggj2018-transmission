﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InstanceObject : MonoBehaviour {


	public GameObject myObject;
	private MouseGrab mouseGrab;
	public int count = 1;
	private Button myButton;
	private TextMeshProUGUI textCount;
	GameManager gameManager;

	void Start()
	{
		myButton = GetComponent<Button>	();
		myButton.onClick.AddListener(TaskOnClick);
		mouseGrab = Camera.main.GetComponent<MouseGrab> ();
		textCount = GetComponentInChildren<TextMeshProUGUI> ();
		textCount.text = count.ToString ();
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
	}

	// Update is called once per frame
	void TaskOnClick(){
		if (gameManager.gameClickState == ClickStates.ClickInstanciate) {
			if (count > 0) {
				//Debug.Log (myObject.ToString());
				Vector3 mousePos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 5f);
				GameObject transmitter = Instantiate (myObject) as GameObject;
				transmitter.transform.position = Camera.main.ScreenToWorldPoint (mousePos);
				mouseGrab.SetGrabbedObject (transmitter);
				gameManager.gameClickState = ClickStates.ClickDrop;
				count--;
				textCount.text = count.ToString ();
			} 
			if (count == 0) {
				Vector3 mousePos = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 5f);
				GameObject smoke = Instantiate (Resources.Load ("LandSmoke")) as GameObject;
				smoke.transform.position = Camera.main.ScreenToWorldPoint (mousePos);
				myButton.gameObject.SetActive (false);
			}
		}
	}
}
