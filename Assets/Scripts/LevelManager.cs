﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public GameObject[] levelItems;
    private GameManager gameManager;
    private List<AntennaController> antennasList = new List<AntennaController>();
	public GameObject winImage;

	// Use this for initialization
	void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.SetLevelManager(this);

        foreach (GameObject antenna in levelItems)
        {
            AntennaController newAntennaController = antenna.GetComponent<AntennaController>();
            antennasList.Add(newAntennaController);            
        }
        Debug.Log("first antennas coount: " + antennasList.Count);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void LateUpdate()
    {
        CheckWin();
    }

    public void SetItemsOnGUI()
    {
        Debug.Log("hola");
    }

    /// <summary>
    /// Checks if all the antennas are connected
    /// </summary>
    public void CheckWin()
    {
        Debug.Log("checking win!");
        int antennasConnected = 0;
        Debug.Log("game state: " + gameManager.gameState);
        if (gameManager.gameState == GameStates.PlayingLevel)
        {
          //  Debug.Log("items: " + levelItems.Length);
			Debug.Log("Paso Game state");
			Debug.Log ("Atenna 1 : "+antennasList [0].GetIsConnected ());
			Debug.Log ("Atenna 2 : "+antennasList [1].GetIsConnected ());
			if (antennasList [0].GetIsConnected () && antennasList [1].GetIsConnected ()) {
				winImage.SetActive (true);
				Debug.Log("YOU WON!!!");
				gameManager.LevelWon();
			}



            /*foreach (AntennaController antenna in antennasList)
            {
				if (antenna.GetIsConnected ()) {
					antennasConnected++;
					Debug.Log("entro a is connected");
				}
				else {
					Debug.Log("return");
					return;
				}
                Debug.Log("antennas connected: " + antennasConnected + " antennas count: " + antennasList.Count);
                
                if (antennasConnected == 2)
                {
                    gameManager.LevelWon();
					winImage = true;
                    Debug.Log("YOU WON!!!");
                    return;
                }
            }   */         
        }
    }
}
