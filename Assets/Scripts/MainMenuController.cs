﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {

    public string playSceneName = "testMundo";
    public GameObject mainPanel;
    public GameObject creditsPanel;
    public GameObject helpPanel;

    private AudioSource musicAudioSource;
    private AudioSource buttonAudioSource; 

    public AudioClip mainMenuTheme;
    public AudioClip regutalButton;
    public AudioClip playButton; 
        
	// Use this for initialization
	void Start () {
        mainPanel.SetActive(true);
        creditsPanel.SetActive(false);
        helpPanel.SetActive(false);

        musicAudioSource = transform.Find("MusicAudioSource").GetComponent<AudioSource>();
        buttonAudioSource = transform.Find("ButtonAudioSource").GetComponent<AudioSource>();
	}		    

    public void PlayGame()
    {
        buttonAudioSource.PlayOneShot(playButton);
        StartCoroutine(ChangeSceneDelay(playButton.length*1.1f));
    }

    IEnumerator ChangeSceneDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Debug.Log("CHANGE SCENE!");
        SceneManager.LoadScene(playSceneName);
    }

    public void ShowCredits()
    {
        PlayRegultarButtonAudio();
        mainPanel.SetActive(false);
        creditsPanel.SetActive(true);        
    }

    public void HideCredits()
    {
        PlayRegultarButtonAudio();
        mainPanel.SetActive(true);
        creditsPanel.SetActive(false);        
    }

    public void ShowHelp()
    {
        PlayRegultarButtonAudio();
        mainPanel.SetActive(false);
        helpPanel.SetActive(true);        
    }

    public void HideHelp()
    {
        PlayRegultarButtonAudio();
        mainPanel.SetActive(true);
        helpPanel.SetActive(false);        
    }

    private void PlayRegultarButtonAudio()
    {
        buttonAudioSource.PlayOneShot(regutalButton);
    }

    public void CloseGame()
    {
        PlayRegultarButtonAudio();
        Application.Quit();
    }
}
