﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates
{
    MainMenu,
    CreateLevel,
    PlayingLevel,
    EndLevelGUI,
    ChangingLevel,
}

public enum ClickStates{
	ClickInstanciate,
	ClickDrop,
	ClickAntennaView
}

public class GameManager : MonoBehaviour {
    public GameStates gameState;
	public ClickStates gameClickState = ClickStates.ClickInstanciate;
    LevelManager levelManager;
	private AudioSource aud;
	public GameObject canvasHUD;
	public TimerController timer;
	bool winn =false;
	float time = 4f;

	// Use this for initialization
	void Start () {
        //DontDestroyOnLoad(gameObject);
		gameState = GameStates.CreateLevel;
		aud = GameObject.FindGameObjectWithTag ("myMusic").GetComponent<AudioSource>();
		aud.Play ();
		canvasHUD.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		if (gameState == GameStates.PlayingLevel) {
			timer.startTimer = true;
		}

		if (winn) {
			if (time > 0f) {
				time -= Time.deltaTime;
			} else {
				int nextScene = SceneManager.GetActiveScene ().buildIndex +1;
				if (SceneManager.GetActiveScene ().name == "testMundo2") {
					SceneManager.LoadScene ("testMundo");
				} else {
					SceneManager.LoadScene(0);
				}
			}
		}
	}

    void CreateLevel()
    {

   //     gameState = GameStates.ShowingStartLevelGUI;
    }

    public void LevelWon()
    {
        gameState = GameStates.EndLevelGUI;
		winn = true;
        //TODO: deactivate input for planet 
    }

    public void SetLevelManager(LevelManager newLevelManager)
    {
        levelManager = newLevelManager;
        CreateLevel();
    }    

	public void LevelLose(){
		//cortar musica ,GUI
		gameState = GameStates.EndLevelGUI;
		aud.Stop ();
		canvasHUD.SetActive (false);
	}
	public void StartPlay (){
		gameState = GameStates.PlayingLevel;
	}
}
