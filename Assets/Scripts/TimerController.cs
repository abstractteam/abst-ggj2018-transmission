﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class TimerController : MonoBehaviour {

    public float levelTime = 30f;
    float timer = 0f;
	float timer2 = 0f;
	public float videoDuration =  15f;
    GameManager gameManager;
    bool endMessageSended = false;
	public MissileController missilC;
	public bool startTimer = false;
	public TextMeshProUGUI timerText; 
	// Use this for initialization
	void Start ()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (gameManager == null)
            Debug.Log("game manager not found", this);

        timer = levelTime;
	}
	
	// Update is called once per frame
	void Update ()
	{		
		if (startTimer) {
			if (timer > 0f) {
				timer -= Time.deltaTime;
				timerText.text = timer.ToString ();
			} else if (!endMessageSended) {
				timer = 0f;
				//TODO add this 
				//gameManager.TimeEnded();
				missilC.testLaunch = true;
				endMessageSended = true;
				gameManager.LevelLose ();
			}
			if (endMessageSended) {
				if (timer2 < videoDuration) {
					timer2 += Time.deltaTime;
				} else {
					SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
				}
			}
		}
	}
}
