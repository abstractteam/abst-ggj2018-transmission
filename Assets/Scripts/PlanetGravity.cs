﻿using UnityEngine;
using System.Collections;

public class PlanetGravity : MonoBehaviour {

	//[Range(1f,30f)]
	//public float gravity = 10f;

	public void Attract(Transform body,Vector3 targetPoint, float gravity){
		Vector3 targetDir = (body.position - targetPoint).normalized;
		RotateToPlanet (body);
		body.GetComponent<Rigidbody>().AddForce(targetDir * -gravity);
	}
	public void RotateToPlanet(Transform body){
		Vector3 targetDir = (body.position - transform.position).normalized;
		Vector3 bodyUp = body.up;
		body.rotation = Quaternion.FromToRotation(bodyUp, targetDir) *body.rotation;
	}
}

