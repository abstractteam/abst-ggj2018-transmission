﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Transmitter
{
    BroadcastAntenna,
    DirectionalAntenna,
    SatelliteClose,
    SatelliteStationary
}

public class AntennaController : MonoBehaviour {

    public enum AntennaStates
    {
        Waiting,
        SettingRotation
    }

    public AntennaStates antennaState;

    public Transmitter transmitter;
    public float reachDistance = 10f;
    public bool isConnected = false;

    [Tooltip ("The child where the signal originates")]
    public GameObject signalSender;
    [Tooltip ("The mesh that will turn green when connected")]
    public MeshRenderer toColorRenderer;

    Color originalColor;
    public Color connectedColor;

    public bool canRotateOnX = true;
    public bool canRotateOnY = true;

    public Transform forwardVector;

    public SpriteRenderer rippleRenderer;
    Vector3 originalBoxSize;
    Vector3 halfBoxSize;

    public bool checkInterference = false;

    //-------- Input variables --------
    float horizRotate = 0.0f;
    float sensitivityX = 15.0f;
    float vertRotate = 0f;
    float sensitivityY = 15.0f;
	GameManager gameManager;

	public Transform cameraPoint;


    // Use this for initialization
    void Start()
    {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
        if (forwardVector == null && transmitter == Transmitter.DirectionalAntenna)
        {
            Debug.Log(gameObject.name + " needs a forward vector, assignig gameobject as default", this);
            forwardVector = transform;
        }

        if (senderTransform != null)
        {
            originalBoxSize = senderTransform.localScale;
            halfBoxSize = new Vector3(senderTransform.localScale.x, senderTransform.localScale.y, senderTransform.localScale.z / 2);
        }
        else
            Debug.Log("sender transform not attached", this);

        if (transmitter == Transmitter.DirectionalAntenna || transmitter == Transmitter.DirectionalAntenna)
        {
            if (forwardVector != null && senderTransform != null)
                checkInterference = true;
            else
                Debug.Log("forward vector or sender transform missing", this);
        }

        originalColor = toColorRenderer.material.color;

        if (signalSender == null)
            signalSender = gameObject;

        SetConnectionColor();
    }

    void Update ()
    {
		//MyOnMouseOver ();
        if(checkInterference)
            CheckInterference();
        CheckRotateAntenna();
    }

    void CheckInterference()
    {
        RaycastHit hit;
        Debug.DrawRay(senderTransform.position, senderTransform.forward * 100, Color.blue);

        if (Physics.Raycast(senderTransform.position, senderTransform.forward, out hit, 100))
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Interference"))
            {
              //  Debug.Log("interference!");
                senderTransform.localScale = halfBoxSize;
            }
        }
        else
            senderTransform.localScale = originalBoxSize;
    }

    void CheckRotateAntenna()
    {
        if (antennaState == AntennaStates.SettingRotation)
        {
            if (canRotateOnX)
            {
                horizRotate += Input.GetAxis("Horizontal") * sensitivityX * Time.deltaTime;
              //  Debug.Log("horiz rotate: " + horizRotate);
            }
            else
                horizRotate = 0f;

            if (canRotateOnY)
            {
                vertRotate += Input.GetAxis("Vertical") * sensitivityY * Time.deltaTime;
              //  Debug.Log("vert rotate: " + vertRotate);
                vertRotate = Mathf.Clamp(vertRotate, -90, 90);
            }
            else
                vertRotate = 0f;

			if (transmitter == Transmitter.DirectionalAntenna) {
				Vector3 eulerVector = new Vector3(-90, vertRotate, horizRotate);
				signalSender.transform.localRotation = Quaternion.Euler (eulerVector);
			} else if (transmitter == Transmitter.SatelliteClose) {
				signalSender.transform.localRotation = Quaternion.Euler (vertRotate, horizRotate, 0);
			}
        }

        if (Input.GetKey("mouse 1") && antennaState == AntennaStates.SettingRotation)
        {
            ChangeState();
			gameManager.gameClickState = ClickStates.ClickInstanciate;
			CameraControl cc = Camera.main.GetComponent<CameraControl>();
			cc.ResetCamera ();
        }
    }

    void OnMouseOver()
    {
		print ("Box Clicked!");
		if (Input.GetKeyDown (KeyCode.Space)) {
			Debug.Log ("entro a space");
			//if (Input.GetKey (KeyCode.Space)) {
				//gameManager.gameClickState = ClickStates.ClickAntennaView;
			CameraControl cc = Camera.main.GetComponent<CameraControl>();
			cc.SetCamera (cameraPoint);
				ChangeState ();
				print ("Box Clicked with mouse 0!");
			//}
		}
    }
	/*
	void MyOnMouseOver(){
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit hit;
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (Physics.Raycast (ray, out hit, 100)) {
				AntennaController aC = hit.collider.gameObject.GetComponent<AntennaController> ();
				if(aC!=null){
					if (aC.myID == myID) {
						Debug.Log ("OverObject!");
						gameManager.gameClickState = ClickStates.ClickAntennaView;
						ChangeState ();
					}
				}
			}
		}
	}
	*/
    public void ChangeState()
    {
		//if (gameManager.gameClickState == ClickStates.ClickAntennaView) {
			if (antennaState == AntennaStates.SettingRotation)
				antennaState = AntennaStates.Waiting;
			else if (antennaState == AntennaStates.Waiting)
				antennaState = AntennaStates.SettingRotation;
		//}
    }

    private void SetConnectionColor()
    {
        if (isConnected)
        {            
            toColorRenderer.material.color = connectedColor;
        }
        else
        {            
            toColorRenderer.material.color = originalColor;
        }

        if (rippleRenderer != null)
        {
            if (senderTasks > 0)
                rippleRenderer.material.color = connectedColor;
            else
                rippleRenderer.material.color = Color.red;
        }
        else
            Debug.Log("ripple renderer not found", this);
    }

    public void SetIsConnected(bool newValue)
    {
        isConnected = newValue;
        SetConnectionColor();
    }

    public bool GetIsConnected()
    {
        if (isConnected)
            return true;
        else
            return false;
    }

    int amountOfConnections = 0;
    List<GameObject> objectConnected = new List<GameObject>();
    
    //wheter the sender has detected one or more antennas to connect
    int senderTasks = 0;
    public Transform senderTransform;

    bool hasInterference;
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Receiver"))
            senderTasks++;

        if (other.gameObject != objectConnected.Contains(other.gameObject) 
            && other.gameObject.layer != LayerMask.NameToLayer("Interference") 
            && other.gameObject.layer != LayerMask.NameToLayer("Default")            
			&& other.gameObject.layer != LayerMask.NameToLayer("Earth")            
			&& other.gameObject.layer != LayerMask.NameToLayer("Water")            
			&& other.gameObject.layer != LayerMask.NameToLayer("Orbit")            
			&& other.gameObject.layer != LayerMask.NameToLayer("Orbitter")            
			&& other.gameObject.layer != LayerMask.NameToLayer("Grab") 
            )
        {
            //Debug.Log("adding: " + other.gameObject.name + " to the list");
			//Debug.Log ("paremtt: " + other.transform.parent.gameObject.name);
            objectConnected.Add(other.transform.parent.gameObject);
			amountOfConnections++;
            SetIsConnected(true);
        }
    
//        Debug.Log("other layer: " + LayerMask.LayerToName(other.gameObject.layer)+ " i am: "+gameObject.name);       
    }
    
    void OnTriggerExit(Collider other)
	{        
		if (other.gameObject.layer != LayerMask.NameToLayer ("Default")) {
			if (other.gameObject.layer == LayerMask.NameToLayer ("Receiver"))
				senderTasks--;

			Debug.Log ("removing: " + other.gameObject.name + " from the list");
			amountOfConnections--;
			objectConnected.Remove (other.gameObject);      

			if (amountOfConnections == 0)
				SetIsConnected (false);
		}
	}
}
