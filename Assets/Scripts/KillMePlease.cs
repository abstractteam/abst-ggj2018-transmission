﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMePlease : MonoBehaviour {

	private PlanetGravity planet;

	// Update is called once per frame
	void Start ()
	{
		planet = GameObject.FindGameObjectWithTag ("Earth").GetComponent<PlanetGravity>();
		Destroy (this.gameObject,3f);
	}

	// Start is called
	void Update(){
		planet.RotateToPlanet (transform);
	}

}
