﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	[Range(-10f,10f)]
	public float cameraVelocityX = 0f;
	public float cameraVelocityY = 0f;
	public float scrollVelocity = 0f;
	public float cameraZoomVelocity = 0f;
	public Vector2 zoomRange;
	private float cameraInitialZoom = 0;
	private GameObject planetObject;

	// grab camera rotation variables
	private Vector3 onClickPoint;
	private Vector3 onReleasePoint;
	private float dist;
	public float desaceleration = 0.1f;    

	[Range(0f,5f)]
	public float handDeltaVelocity = 0f;
	private float handDeltaX = 0f;
	private float handDeltaY = 0f;
	private Vector3 handDeltaVector;

	public bool userCameraControl = true;
	private Vector3 originalPosition;
	private Quaternion originalRotation;

	public GameObject canvasHud;
	//public GameObject canvasHelp;

	void Start () 
	{
		planetObject = GameObject.FindGameObjectWithTag ("Earth");
	}

	void LateUpdate()
	{
		if (userCameraControl) {
			canvasHud.SetActive (true);
			GetZoomScroll ();
			GetClickCameraGrab ();
		} else {
			canvasHud.SetActive (false);
		}
	}

	void FixedUpdate () 
	{
		if (userCameraControl) 
		{
			if (planetObject != null) 
			{
				RotateAroundPlanet (planetObject);
			}
		}
	}

	void RotateAroundPlanet(GameObject lookTarget){
		transform.RotateAround (planetObject.transform.position+handDeltaVector, transform.up, cameraVelocityX);
		transform.RotateAround (planetObject.transform.position+handDeltaVector, transform.right, -cameraVelocityY);
		transform.position += transform.forward * cameraZoomVelocity;
		transform.position += handDeltaVector;
	}

	void GetZoomScroll()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") < 0f) 
		{ // forward
			cameraZoomVelocity += scrollVelocity;
		} else if (Input.GetAxis ("Mouse ScrollWheel") > 0f) 
		{
			cameraZoomVelocity -= scrollVelocity;
		} 
		else 
		{
			cameraZoomVelocity = 0f;
		}
	}

	void GetClickCameraGrab()
	{
		if (Input.GetButton ("Fire2")) 
		{
			if (Input.GetKey (KeyCode.LeftControl)) 
			{
				handDeltaX = Input.GetAxis ("Horizontal") * -handDeltaVelocity;
				handDeltaY = Input.GetAxis ("Vertical") * -handDeltaVelocity;
				//handDeltaVector = new Vector3 (handDeltaX ,handDeltaY,0f)
				handDeltaVector = transform.up * handDeltaY + transform.right * handDeltaX;
			} 
			else 
			{
				handDeltaVector = Vector3.zero;
				float mouseVelocityX = Input.GetAxis ("Horizontal") * scrollVelocity;
				cameraVelocityX = mouseVelocityX;

				float mouseVelocityY = Input.GetAxis ("Vertical") * scrollVelocity;
				cameraVelocityY = mouseVelocityY;
			}
		} 
		else 
		{
			cameraVelocityX = 0f;
			cameraVelocityY = 0f;
			handDeltaVector = Vector3.zero;
		}
	}

	public void SetCamera(Transform pos)
	{
		if (pos != null) 
		{
			userCameraControl = false;
			originalPosition = transform.position;
			originalRotation = transform.rotation;
			transform.position = pos.position;
			transform.rotation = pos.rotation;
			transform.parent = pos;
		}
	}

	public void ResetCamera()
	{
		transform.parent = null;
		transform.position = originalPosition;
		transform.rotation = originalRotation;
		userCameraControl = true;
	}
}
