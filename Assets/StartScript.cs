﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartScript : MonoBehaviour {

	public float timeToStartFade = 1f;
	private float timeElapsed = 0f;
	public GameObject imagen;


	void Update () {
		if (timeElapsed < timeToStartFade) {
			timeElapsed += Time.deltaTime;
		} else {
			imagen.SetActive (false);
		}
	}
}
