﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameHelpCanvasController : MonoBehaviour {

    public GameObject helpPanel;
    public GameObject helpButton;
    public GameObject HUDCanvas;

	// Use this for initialization
	void Start () {
        helpButton.SetActive(true);

        if(helpPanel != null)
            helpPanel.SetActive(false);

        if(HUDCanvas != null)
			HUDCanvas.SetActive(true);  
	}

    public void ShowHelpPanel()
    {
        helpPanel.SetActive(true);
        if(HUDCanvas != null)
            HUDCanvas.SetActive(false);

        if (helpButton != null)
            helpButton.SetActive(false);
    }

    public void HideHelpPanel()
    {
        helpPanel.SetActive(false);
        if (HUDCanvas != null)
            HUDCanvas.SetActive(true);

        if (helpButton != null)
            helpButton.SetActive(true);
    }    
}
