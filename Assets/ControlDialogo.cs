﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlDialogo : MonoBehaviour {

	private Image img;
	public Sprite[] spritesDiaologo;
	public int currentSprite = 0;
	public float timer = 2f;
	//private float elapsedTime = 0f;
	private float timerInitial;
	bool ready = false;
	public GameObject startImage;

	void Start () {
		timerInitial = timer;
		img = GetComponent<Image> ();
	}

	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;
		} else {
			timer = timerInitial;
			if (currentSprite < spritesDiaologo.Length - 1) {
				currentSprite++;
			} else {
				if (!ready) {
					GameManager gm = GameObject.Find ("GameManager").GetComponent<GameManager> ();
					gm.StartPlay ();
					ready = true;
					startImage.SetActive (true);
					this.gameObject.SetActive (false);
				}
			}
		}
		img.sprite = spritesDiaologo [currentSprite];
		
	}
}
